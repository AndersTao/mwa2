# Usage

```
docker run -d \
  --name="mwa2-hjaltelin" \
  -p 65001:8000 \
  -v /macdesk/munki/hjaltelin:/munki_repo:rw \
  -v /macdesk/munki/hjaltelin/pkgs/global:/munki_repo/pkgs/global:ro \
  -v /macdesk/munki/hjaltelin/pkgsinfo/global:/munki_repo/pkgsinfo/global:ro \
  -e ADMIN_PASS=@lbino00
  atao/mwa2:latest
```
